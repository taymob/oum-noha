package com.tayamino.oum_noha.backend;

import com.parse.Parse;
import com.parse.ParseObject;

import android.app.Application;

import com.tayamino.oum_noha.object.Recipe;

public class CustomApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Recipe.class);

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("oum-noha")
                // if defined
                .clientKey("9d114265e6044e94")
                .server("https://oum-noha.herokuapp.com/model/api")
                .build()
        );
    }
}
