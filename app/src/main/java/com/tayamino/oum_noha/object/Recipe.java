package com.tayamino.oum_noha.object;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Recipe")
public class Recipe extends ParseObject {
    public String getDisplayName() {
        return getString("displayName");
    }
    public void setDisplayName(String value) {
        put("displayName", value);
    }
}
